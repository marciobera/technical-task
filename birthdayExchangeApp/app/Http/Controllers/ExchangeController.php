<?php

namespace App\Http\Controllers;

use App\Exchange;
use \Carbon\Carbon;
use DB;
use Config;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ExchangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carbonTime = Carbon::now();
        $currentDate = $carbonTime->toDateTimeString();
        $oneYearAgo = $carbonTime->subYear(1)->toDateTimeString();
        
        try {
                
            $exchanges = DB::table('exchanges')
                            ->whereBetween('date', [$oneYearAgo, $currentDate])
                            ->orderBy('date', 'DESC')
                            ->get();
            foreach($exchanges as $exchange){
                $exchange->date = Carbon::parse($exchange->date)->format('jS l Y');
            }

            return response()->json(['error' => false, 'message' => count($exchanges).' birthday exchange(s) data stored.', 'data' => $exchanges], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Something goes wrong.'], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //http://data.fixer.io/api/2018-02-18?access_key=ee5ab5042f6cc23ae2f95362ef7cad9d
        if(!isset($request->date)){
            return response()->json(['error' => true, 'message' => 'Input date is required.'], 400);
        }else{
            
            // list($year,$month,$day) = explode("-", $request->date);

            try {
                
                $carbonTime = Carbon::parse($request->date.' 23:59:59');
                
                if($carbonTime->greaterThan(Carbon::now())) {
                    return response()->json(['error' => true, 'message' => 'The informed date does not be greater than today.'], 400);
                } else
                if($carbonTime->lessThan(Carbon::now()->subYear(1))) {
                    return response()->json(['error' => true, 'message' => 'The informed date does not be less than one year ago.'], 400);
                }

                $exchange = Exchange::where('date', $carbonTime->toDateString())->first();
                
                if(!isset($exchange)){
                  
                    $FIXER_URL = Config::get('constants.FIXER_API_URL') . '/' . $carbonTime->toDateString() . '?access_key=' . Config::get('constants.FIXER_API_KEY');
                    try {
                        $client = new Client();
                        $response = $client->get($FIXER_URL);
                       
                        $exchange = new Exchange;
                        $exchange->date = $carbonTime->toDateString();
                        $exchange->requests_quantity = 1;
                        $exchange->response = $response->getBody();
                        $exchange->save();
                            
                    } catch (\Exception $e) {
                        return response()->json(['error' => true, 'message' => 'DATA API does not found.'], 400);
                    }

                    $exchange = Exchange::where('date', $carbonTime->toDateString())->first();
                    
                }else{
                    //update counter
                    $exchange->requests_quantity = $exchange->requests_quantity + 1;
                    $exchange->update();
                }

                return response()->json(['error' => true, 'message' => Carbon::parse($exchange->date)->format('jS l Y'), 'data' => $exchange->response], 200);

            } catch (\Exception $e) {
                return response()->json(['error' => true, 'message' => 'Invalid date.'], 400);
            }
            
            return response()->json(['error' => true, 'message' => $request->date], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
